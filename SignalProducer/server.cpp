#include "server.h"

#include <QDataStream>
#include <QTcpSocket>
#include <QtCore/QRandomGenerator>

Server::Server(quint16 port, QObject *parent) : QObject(parent)
{
    tcpServer.listen(QHostAddress::LocalHost, port);

    connect(&tcpServer, &QTcpServer::newConnection,
            this, &Server::acceptConnection);
    connect(&timer, &QTimer::timeout,
            this, &Server::sendNewValue);
}

void Server::acceptConnection()
{
    QTcpSocket *connection = tcpServer.nextPendingConnection();

    connect(connection, &QTcpSocket::disconnected,
                this, &Server::deleteConnection);

    connections.push_back(connection);
    startBroadcasting();
}

void Server::deleteConnection()
{
    QTcpSocket *connection = qobject_cast<QTcpSocket *>(QObject::sender());

    connections.removeOne(connection);
    connection->deleteLater();
}

void Server::startBroadcasting()
{
    if (!timer.isActive())
    {
        timer.setInterval(1000);
        timer.start();
    }
}

void Server::sendNewValue()
{
    qreal newValue = QRandomGenerator::global()->bounded(5) - 2.5;

    for (const auto& connection : connections)
    {
        QDataStream stream(connection);
        stream << newValue;
    }
}

Server::~Server()
{
    tcpServer.close();
    timer.stop();
}

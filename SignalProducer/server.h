#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTimer>
#include <QList>

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(quint16 port, QObject *parent = nullptr);
    ~Server();

signals:

private slots:
    void acceptConnection();
    void deleteConnection();

    void sendNewValue();

private:
    QTcpServer tcpServer;
    QList<QTcpSocket *> connections;
    QTimer timer;

    void startBroadcasting();
};

#endif // SERVER_H

#include "server.h"
#include <QCoreApplication>

static const quint16 PORT = 48654;

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    Server server(PORT);

    return app.exec();
}

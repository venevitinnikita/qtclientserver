#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(quint16 port, QObject *parent = nullptr);
    void connectToHost(const QString &host);

signals:
    void newValueReceived(qreal value);

private slots:
    void readNewValue();

private:
    quint16 port;
    QTcpSocket connection;
};

#endif // CLIENT_H

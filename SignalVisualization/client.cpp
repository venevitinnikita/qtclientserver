#include "client.h"

#include <QHostAddress>
#include <QtCore/QDataStream>

Client::Client(quint16 _port, QObject *parent) : QObject(parent), port(_port)
{
    connect(&connection, &QIODevice::readyRead, this, &Client::readNewValue);
}

void Client::connectToHost(const QString &host)
{
    connection.close();
    connection.connectToHost(host, port);
}

void Client::readNewValue()
{
    qreal newValue;

    QDataStream stream(&connection);
    stream.startTransaction();

    stream >> newValue;

    stream.commitTransaction();

    emit newValueReceived(newValue);
}

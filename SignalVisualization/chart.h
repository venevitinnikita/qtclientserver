#ifndef CHART_H
#define CHART_H

#include <QtCharts/QChart>
#include <QElapsedTimer>

QT_CHARTS_BEGIN_NAMESPACE
class QSplineSeries;
class QValueAxis;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

class Chart: public QChart
{
    Q_OBJECT
public:
    Chart(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
    void setAutoscrollingEnabled(bool enabled);
    bool isAutoscrollingEnabled();
    void zoomHorizontally(qreal factor);
    void zoomVertically(qreal factor);
    void drawNewValue(qreal value);
    virtual ~Chart();

private:
    QSplineSeries *m_series;
    QStringList m_titles;
    QValueAxis *m_axisX;
    QValueAxis *m_axisY;
    qreal m_step;
    qreal m_x;
    qreal m_y;
    QElapsedTimer timer;
    bool autoscrollingEnabled;
};

#endif /* CHART_H */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QApplication>
#include <QtCharts/QChartView>

static const QString LOCALHOST = "127.0.0.1";
static const quint16 PORT = 48654;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    chart = new Chart;
    chart->legend()->hide();

    chartView = new ChartView(chart, ui->centralWidget);
    chartView->setRenderHint(QPainter::Antialiasing);
    ui->gridLayout->addWidget(chartView, 1, 0, 2, 2);

    client = new Client(PORT);

    connect(client, &Client::newValueReceived, chart, &Chart::drawNewValue);
    connect(ui->connectBtn, &QPushButton::clicked, [&](){
        QString host = ui->ipFld->text();
        client->connectToHost(host);
    });

    connect(ui->btnUp, &QPushButton::clicked, [&](){ chartView->handleKeyPress(Qt::Key_Up); });
    connect(ui->btnDown, &QPushButton::clicked, [&](){ chartView->handleKeyPress(Qt::Key_Down); });
    connect(ui->btnLeft, &QPushButton::clicked, [&](){ chartView->handleKeyPress(Qt::Key_Left); });
    connect(ui->btnRight, &QPushButton::clicked, [&](){ chartView->handleKeyPress(Qt::Key_Right); });
    connect(ui->btnZoomInX, &QPushButton::clicked, [&](){ chart->zoomHorizontally(0.75); });
    connect(ui->btnZoomInY, &QPushButton::clicked, [&](){ chart->zoomVertically(0.75); });
    connect(ui->btnZoomOutX, &QPushButton::clicked, [&](){ chart->zoomHorizontally(1.25); });
    connect(ui->btnZoomOutY, &QPushButton::clicked, [&](){ chart->zoomVertically(1.25); });
    connect(ui->btnReset, &QPushButton::clicked, [&](){ chartView->handleKeyPress(Qt::Key_Escape); });

    ui->ipFld->setText(LOCALHOST);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete client;
}

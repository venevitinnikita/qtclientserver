#include "mainwindow.h"
#include "chart.h"
#include "chartview.h"
#include "client.h"
#include <QApplication>
#include <QtCharts/QChartView>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    MainWindow window;
    window.show();

    return app.exec();
}

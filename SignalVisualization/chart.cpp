#include "chart.h"
#include <QtCharts/QAbstractAxis>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QValueAxis>
#include <QtCore/QRandomGenerator>
#include <QtCore/QDebug>

Chart::Chart(QGraphicsItem *parent, Qt::WindowFlags wFlags):
    QChart(QChart::ChartTypeCartesian, parent, wFlags),
    m_series(nullptr),
    m_axisX(new QValueAxis()),
    m_axisY(new QValueAxis()),
    m_step(0),
    m_x(0),
    m_y(1),
    autoscrollingEnabled(true)
{
    m_series = new QSplineSeries(this);
    QPen green(Qt::red);
    green.setWidth(3);
    m_series->setPen(green);

    addSeries(m_series);

    addAxis(m_axisX,Qt::AlignBottom);
    addAxis(m_axisY,Qt::AlignLeft);
    m_series->attachAxis(m_axisX);
    m_series->attachAxis(m_axisY);
    m_axisX->setTickCount(5);
    m_axisX->setRange(-10, 10);
    m_axisY->setRange(-5, 5);
}

void Chart::zoomHorizontally(qreal factor)
{
    QRectF rect = plotArea();
    rect.setWidth(factor * rect.width());
    rect.moveCenter(plotArea().center());
    zoomIn(rect);
}

void Chart::zoomVertically(qreal factor)
{
    QRectF rect = plotArea();
    rect.setHeight(factor * rect.height());
    rect.moveCenter(plotArea().center());
    zoomIn(rect);
}

Chart::~Chart()
{

}

void Chart::setAutoscrollingEnabled(bool enabled)
{
    autoscrollingEnabled = enabled;
}

bool Chart::isAutoscrollingEnabled()
{
    return autoscrollingEnabled;
}

void Chart::drawNewValue(qreal value)
{
    if (!timer.isValid())
    {
        timer.start();
    }

    qreal newX = timer.elapsed() / 1000.0;
    m_x = newX;
    m_y = value;
    m_series->append(m_x, m_y);

    if (autoscrollingEnabled)
    {
        qreal currentCenter = (m_axisX->max() + m_axisX->min()) / 2.0;
        qreal dx = newX - currentCenter;

        qreal pixelValueRatio = plotArea().width() / (m_axisX->max() - m_axisX->min());

        scroll(dx * pixelValueRatio , 0);
    }
}

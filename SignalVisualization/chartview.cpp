#include "chartview.h"
#include "chart.h"
#include <QtGui/QMouseEvent>

ChartView::ChartView(QChart *chart, QWidget *parent) :
    QChartView(chart, parent)
{
}

void ChartView::keyPressEvent(QKeyEvent *event)
{
    if (!handleKeyPress(event->key()))
    {
        QGraphicsView::keyPressEvent(event);
    }
}

bool ChartView::handleKeyPress(int key)
{
    Chart *customChart = static_cast<Chart *>(chart());

    bool handled;
    bool autoscrollingEnabled = customChart->isAutoscrollingEnabled();

    switch (key) {
    case Qt::Key_Plus:
        chart()->zoomIn();
        handled = true;
        break;
    case Qt::Key_Minus:
        chart()->zoomOut();
        handled = true;
        break;
    case Qt::Key_Left:
        chart()->scroll(-10, 0);
        handled = true;
        autoscrollingEnabled = false;
        break;
    case Qt::Key_Right:
        chart()->scroll(10, 0);
        handled = true;
        autoscrollingEnabled = false;
        break;
    case Qt::Key_Up:
        chart()->scroll(0, 10);
        handled = true;
        break;
    case Qt::Key_Down:
        chart()->scroll(0, -10);
        handled = true;
        break;
    case Qt::Key_Escape:
        chart()->zoomReset();
        handled = true;
        autoscrollingEnabled = true;
        break;
    default:
        handled = false;
        break;
    }

    customChart->setAutoscrollingEnabled(autoscrollingEnabled);

    return handled;
}
